import { Slider } from './BEM/slider/slider.class.js';
import { Tab } from './BEM/tab/tab.class.js';
import { Map } from './BEM/map/map.class.js';

let item = document.querySelectorAll('.slider');

for( let i = 0; i < item.length; i++ ){
    let obj = new Slider( item[i] );
}

let tabs = document.querySelectorAll('.tab');

for( let i = 0; i < tabs.length; i++ ){
    let obj = new Tab( tabs[i] );
}

let modals = document.querySelectorAll('img[data-modal]');

for (var i = 0; i < modals.length; i++) {
    let image = modals[i];
    image.addEventListener( 'click', (e) => {
        let clone = image.cloneNode(false);
        let modalBlock = document.querySelector('.modal');
        modalBlock.appendChild(clone);
        setTimeout( () => {
            modalBlock.classList.add('modal_active');
            modalBlock.style.zIndex = '100';
        }, 0);
        document.body.style.overflow = 'hidden';
    });
}

let modalBlock = document.querySelector('.modal');
modalBlock.innerHTML = '';
modalBlock.addEventListener( 'click', (e) => {
    modalBlock.classList.remove('modal_active');
    setTimeout( () => {
        modalBlock.innerHTML = '';
        modalBlock.style.zIndex = '-1';
        document.body.style.overflow = 'inherit';
    }, 500);
});

// let map = document.querySelector('#map');
// console.log(map);
// let map_obj = new Map( map );