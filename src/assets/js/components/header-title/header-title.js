import $ from 'jquery';

var header_resize = function header_resize(){
	var a = $('.header').height();
	var b = $('.header__navigation').height();
	var rem = parseInt( $('html').css('font-size') );
	var c = a - b - 11 * rem;
	var h = $('.header-title__content').height();
	var height = ( c > h ) ? c : h;
	$('.header__header-title').height( height );
}

header_resize();

$(window).on('resize', header_resize);