import $ from 'jquery';

var count;

$(document).ready(function(){
    var owl = $('#owl-feedback').owlCarousel({
        loop:true,
        margin:30,
        stagePadding: 0,
        nav:true,
        dots:false,
        callbacks :  true,
        lazyLoad:true,
        navText: ['<i class="icon-arrow_right"></i>','<i class="icon-arrow_right"></i>'],
        responsive:{
            0:{
                items:1,
                nav: false,
            },
            800:{
                items:1,
                nav: false,
            },
            1000:{
                items:3
            }
        }
        });

    make_count();

    $('#owl-feedback').on('translated.owl.carousel', function(event) {
        if (count != 1){
            var prev = "ad";
            $(this).find('.is-center').each(function(index, element){
                $(element).removeClass('is-center');
            });
            $(this).find('.active').each(function(index, element){
            if ( $(element).next().hasClass('active') ){
                if ( $(element).prev().hasClass('active') ){
                    prev = $(element);
                } else {
                    // $(element).children().first().addClass('is-center');
                }
            } 
            });
            if (prev != 'ad')
                prev.children().first().addClass('is-center');
        } else {
            $(this).find('.item').each(function(index, element){
                $(element).addClass('is-center');
            });
        }
    })

    owl.owlCarousel();
    // Listen to owl events:
    owl.on('changed.owl.carousel', function(event) {
        if (count == 1){
            setTimeout(function(){
                $('#owl-feedback').height( $( "#owl-feedback .owl-item.active" ).height() ), 
                200
            });
        }
    });
    var max = 300;
    if (count > 1){
        $('#owl-feedback').find('.owl-item.active').each(function(index, element){
            if ( $(element).height() > max )
                max = $(element).height();
        });
        $('#owl-feedback').height( max );
    }
    
});

$(window).resize(function(){
    make_count();
});

function make_count(){
    count = 0;
    $('#owl-feedback').find('.active').each(function(){
        count++;
    });
    if (count == 1){
        $('#owl-feedback').find('.active').children().first().addClass('is-center');
    }
}