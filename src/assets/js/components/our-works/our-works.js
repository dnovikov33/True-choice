import $ from 'jquery';

$(document).ready(function(){
  $('#our-works').owlCarousel({
    loop:false,
    margin:10,
    stagePadding: 0,
    nav:true,
    dots:false,
    callbacks :  true,
    lazyLoad:true,
    navText: ['<i class="icon-arrow_right"></i>','<i class="icon-arrow_right"></i>'],
    responsive:{
        0:{
            items:1
        },
        800:{
            items:2
        },
        1000:{
            items:3
        }
    }
    });

});