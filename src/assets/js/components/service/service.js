import $ from 'jquery';

var service_resize = function(){
	$('.service').each(function(index,item){
		var w = $(item).width();
		$(item).height(w);
	});
}

var service_content = function(){
	$('.service').each(function(index,item){
		var title    = $(item).find('.service__title').outerHeight();
		var subtitle = $(item).find('.service__subtitle').height();

		if( title < subtitle ){
			var m = subtitle - title;
			$(item).find('.service__title').css({ 'margin-top' : m + 'px' });
		}else{
			var m = title - subtitle;
			$(item).find('.service__subtitle').css({ 'margin-bottom' : m + 'px' });
		}
	});
}

service_resize();
service_content();

$(window).on('resize', service_resize);
$(window).on('resize', service_content);