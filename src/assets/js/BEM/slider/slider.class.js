class Slider {
    constructor( item ) {
        this.elem = item;
        this.position = 1;
        this.slides = item.querySelectorAll('.slider__slide').length;

        this.restructureSlider();

        this.setSlideWidth();
        this.setDots();

        this.listenDots();

        this.dragn = false;

        this.listeDragon();

        return this;
    }

    restructureSlider(){
        let content = document.createElement('div');
        content.classList.add('slider__content');
        let slides = document.createElement('div');
        slides.classList.add('slider__slides');
        slides.innerHTML = this.elem.innerHTML;
        content.appendChild( slides );

        this.elem.innerHTML = '';
        this.elem.appendChild( content );
    }

    setSlideWidth(){
        let width = this.elem.offsetWidth;
        let slide_width = width * 0.5;

        let slides = this.elem.querySelectorAll('.slider__slide');

        for(let i = 0; i < slides.length; i++) {
            slides[i].style.width = slide_width + 'px';
        }
    }

    setDots(){
        let nav = document.createElement('div');
        nav.classList.add('slider__nav');

        for( let i=0; i < this.slides; i++ ){
            let dot = document.createElement('button');
            dot.classList.add('slider__nav-dot');
            if( i == 0 ){
                dot.classList.add('slider__nav-dot_active');
            }
            dot.type = 'button';
            dot.name = 'nav-dot';
            dot.setAttribute( 'data-slide', (i+1) );
            nav.appendChild( dot );
        }

        this.elem.appendChild(nav);
    }

    listenDots(){
        let self = this;
        this.elem.addEventListener( 'click', (e) => {
            if( e.target.name == 'nav-dot' ){
                self.moveSlide( e.target.getAttribute( 'data-slide' ) );
            }
        });
    }

    listeDragon(){
        this.listenDragonStart();
        this.listenDragonMove();
        this.listenDragonEnd();
    }

    listenDragonStart(){
        let self = this;
        this.elem.querySelector('.slider__slides').addEventListener( 'mousedown', (e) => {
            self.dragn = true;
            let slider = self.elem.querySelector( '.slider__slides' );
            slider.classList.add('slider__slides_drag');
            self.dragStart = e.x;
        });
    }

    listenDragonMove(){
        let self = this;
        document.addEventListener( 'mousemove', (e) => {
            if( self.dragn ){
                let slider = self.elem.querySelector( '.slider__slides' );
                let last = ( slider.style.left ) ? parseInt( slider.style.left ) : 0;
                slider.style.left = ( e.movementX + last ) + 'px';
            }
        });
    }

    listenDragonEnd(){
        let self = this;
        document.addEventListener( 'mouseup', (e) => {
            if( self.dragn ){
                self.dragn = false;
                self.dragEnd = e.x;
                let slider = self.elem.querySelector( '.slider__slides' );
                slider.classList.remove('slider__slides_drag');
                slider.style.left = 0;
                let path = self.dragStart - self.dragEnd;
                if( path > 50 ){
                    self.moveSlide( self.position + 1 );
                }else if( path < -50 ){
                    self.moveSlide( self.position - 1 );
                }
            }
        });
    }

    moveSlide( slide ){
        slide = parseInt( slide );
        let move = ( 100 / this.slides ) * ( slide - 1 );

        if( move >= 0 && move < 100 ){
            this.elem.querySelector('.slider__slides').style.transform = 'translateX( -' + move +'%)';
            this.position = slide;
            let active = this.elem.querySelectorAll( '.slider__nav-dot_active' );
            for (let i = 0; i < active.length; i++) {
                active[i].classList.remove('slider__nav-dot_active');
            }
            this.elem.querySelector('.slider__nav-dot[data-slide="'+slide+'"]').classList.add('slider__nav-dot_active');
        }
    }
}

export {Slider}
