class Map{
	constructor( item ) {
		this.elem = item;
		this.init_map();

		return this;
	}

	init_map(){
		var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(this.elem, {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
	}
}

export { Map }