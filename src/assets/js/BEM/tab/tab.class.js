class Tab {
	constructor(item) {
		this.elem = item;
		this.listenClick();
	}

	listenClick(){
		let self = this;
		this.elem.addEventListener( 'click', (e) => {
			if( e.target.classList.contains('tab__title') ){
				self.toggleTab();
			}
		});
	}

	toggleTab(){
		let title = this.elem.querySelector('.tab__title');
		if (title.classList.contains('tab__title_active')){
			this.hideTab();
		}else{
			this.showTab();
		}
	}

	showTab(){
		let button = this.elem.querySelector('.tab__title');
		button.classList.add('tab__title_active');
		let content = this.elem.querySelector('.tab__content');
		content.classList.add('tab__content_active');
		content.classList.add('tab__content_show');
		setTimeout( ()=>{
			content.classList.remove('tab__content_show');
			}, 500 );
		this.active = true;
	}

	hideTab(){
		let button = this.elem.querySelector('.tab__title');
		button.classList.remove('tab__title_active');
		let content = this.elem.querySelector('.tab__content');
		content.classList.add('tab__content_hide');
		setTimeout( ()=>{
			content.classList.remove('tab__content_hide');
			content.classList.remove('tab__content_active');
			}, 500 );
		this.active = false;
	}
}

export { Tab }
