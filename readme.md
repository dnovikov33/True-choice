# Miracle Template

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (0.12 or greater)
- [Git](https://git-scm.com/)

After cloning repo you need to make command:

```npm install```

And then

```npm start```